# Multistage Docker build
# syntax=docker/dockerfile:1
FROM ubuntu:bionic AS dev
WORKDIR /app
RUN apt-get update -y
# Fix the tzdata issue
# ENV TZ=Europe/Kiev
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV TZ=Africa/Lagos
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get install -y expect

# COPY automate_location_config.sh .
# RUN chmod +x automate_location_config.sh
# RUN ./automate_location_config.sh
RUN apt-get install wget curl -y


# Install Telegraf
RUN wget -qO- https://repos.influxdata.com/influxdb.key | tee /etc/apt/trusted.gpg.d/influxdb.asc >/dev/null \
    source /etc/os-release

RUN echo "deb https://repos.influxdata.com/ubuntu bionic stable" | tee /etc/apt/sources.list.d/influxdb.list
RUN apt-get update -y && apt-get install telegraf


# Install Chronograf
RUN wget https://dl.influxdata.com/chronograf/releases/chronograf_1.9.0_amd64.deb
RUN dpkg -i chronograf_1.9.0_amd64.deb
RUN rm -f chronograf_1.9.0_amd64.deb


# Install InfluxDB
# RUN apt install influxdb
RUN wget https://dl.influxdata.com/influxdb/releases/influxdb2-2.1.1-linux-amd64.tar.gz
RUN tar xvfz influxdb2-2.1.1-linux-amd64.tar.gz
RUN mv influxdb2-2.1.1-linux-amd64/influxd /usr/local/bin/influxd && rm -r influxdb2-2.1.1-linux-amd64.tar.gz influxdb2-2.1.1-linux-amd64


# Install Grafana
RUN apt-get update
RUN apt-get install -y wget
RUN apt-get install -y curl
RUN apt-get install -y apt-transport-https
# Use expect automation here
RUN apt-get install -y software-properties-common 
RUN wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
RUN echo "deb https://packages.grafana.com/oss/deb stable main" | tee -a /etc/apt/sources.list.d/grafana.list
RUN apt-get update
RUN apt-get install grafana -y



# Installing the plugins for grafana
RUN echo "\n\n\nInstalling plugins for grafana.."
RUN grafana-cli plugins install grafana-piechart-panel && \
    grafana-cli plugins install grafana-clock-panel && \
    grafana-cli plugins install ryantxu-ajax-panel && \
    grafana-cli plugins install speakyourcode-button-panel && \
    grafana-cli plugins install marcusolsson-calendar-panel && \
    grafana-cli plugins install integrationmatters-comparison-panel




COPY commands.sh .
RUN mkdir -p scripts
COPY automate_location_config.sh scripts/automate_location_config.sh
COPY entrypoint.sh scripts/entrypoint.sh
RUN chmod +x commands.sh
RUN chmod +x scripts/entrypoint.sh
RUN ls scripts

ENTRYPOINT ["scripts/entrypoint.sh"]
#CMD ["./commands.sh"]











