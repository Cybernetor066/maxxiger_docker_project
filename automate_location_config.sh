#!/usr/bin/expect
# script name: automate_location_config.sh
spawn apt-get install -y software-properties-common

# Then run the script automatically passing automated user inputs using this command:
expect "Geographic area: "
send "1\r"
expect "Time zone: "
send "31\r"
interact