#!/bin/sh

# Building the image
docker build -t cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0 .
docker image prune -f && docker container prune -f

# Pushing and pulling from dockerhub
docker push cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0
docker pull cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0
docker pull cybernetor066/telegraf-chronograf-influxdb-grafana


# Running the container
docker run -d \
  --name influxdb-grafana \
  -p 8888:8888 \
  -p 8086:8086 \
  -p 3000:3000 \
  -v /my_app/influxdb:/var/lib/influxdb \
  -v /my_app/grafana:/var/lib/grafana \
  cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0


# ssh into the container
docker container exec -it influxdb-grafana /bin/bash

# Stopping the container
docker stop influxdb-grafana && docker image prune -f && docker container prune -f










