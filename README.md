Docker image/container with Telegraf (StatsD), ChronoGraf, InfluxDB and Grafana.
Persistence is supported via mounting volumes to a Docker container.

Plugins present for the grafan service includes:
Pie Chart, Clock, Ajax, Button, Calendar, Comparison



# Pushing and pulling from dockerhub
docker push cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0
docker pull cybernetor066/telegraf-chronograf-influxdb-grafana:v1.0
docker pull cybernetor066/telegraf-chronograf-influxdb-grafana






