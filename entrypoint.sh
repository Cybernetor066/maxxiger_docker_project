#!/bin/bash

# Restart grafana server
service grafana-server restart
sleep 7
service grafana-server restart

sleep 3
# Create influxdb service file and user
# useradd -rs /bin/false influxdb
# mkdir /home/influxdb
# chown influxdb /home/influxdb
# mv influxdb2.service /lib/systemd/system/
# service influxdb2 restart
influxd >/dev/null 2>&1 < /dev/null &


sleep 3
service telegraf restart
sleep 3
service chronograf restart
sleep 3

# # Final confirmation for all services
# service --status-all



tail -f /dev/null































